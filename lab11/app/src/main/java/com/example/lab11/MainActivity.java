package com.example.lab11;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(this::buttonPreses);
    }

    private void buttonPreses(View view){
        getIPInfo();
    }

    private boolean validateIPPart(int ipPart){
        return (ipPart >= 0 && ipPart < 256);
    }

    private String getIP() {
        EditText ip1 = (EditText) findViewById(R.id.ip1);
        EditText ip2 = (EditText) findViewById(R.id.ip2);
        EditText ip3 = (EditText) findViewById(R.id.ip3);
        EditText ip4 = (EditText) findViewById(R.id.ip4);
        int sip1 = Integer.parseInt(ip1.getText().toString());
        int sip2 = Integer.parseInt(ip2.getText().toString());
        int sip3 = Integer.parseInt(ip3.getText().toString());
        int sip4 = Integer.parseInt(ip4.getText().toString());
        String response;
        if (sip1 > 0 && sip1 < 256 && validateIPPart(sip2) && validateIPPart(sip3) && validateIPPart(sip4)){
            response = ip1.getText().toString() + "."
                    + ip2.getText().toString() + "."
                    + ip3.getText().toString() + "."
                    + ip4.getText().toString();
        }
        else response = "Invalid";


        return response;
    }

    private void printInfo(IPInfo info) {
        TextView textView = findViewById(R.id.textView);
        String s;

        if(info == null) s = "Failed";
        else {
            s = info.toString();
        }
        textView.setText(s);
    }

    private void getIPInfo() {

        String ip = getIP();
        if(ip.equals("Invalid")) {
            Toast toast = Toast.makeText(getApplicationContext(), "Wprowadź poprawne ip", Toast.LENGTH_SHORT);
            toast.show();
            return;
        }

        ApiInterface apiInterface = ServiceGenerator.creatService(ApiInterface.class, getIP());
        Call<IPInfo>  call = apiInterface.getIPInfo();

        call.enqueue(new Callback<IPInfo>() {
            @Override
            public void onResponse(Call<IPInfo> call, Response<IPInfo> response) {
                printInfo(response.body());
            }

            @Override
            public void onFailure(Call<IPInfo> call, Throwable t) {
                printInfo(null);
            }
        });

    }

}